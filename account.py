# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
import logging
import os.path
import traceback

from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.i18n import gettext
from trytond.exceptions import UserError


__all__ = ['Configuration', 'AccountTax', 'PaymentType',
    'Invoice', 'InvoiceLine']
logger = logging.getLogger(__name__)


class Configuration(metaclass=PoolMeta):
    __name__ = 'account.configuration'
    ediversa_path = fields.Char('eDiversa Path',
        help='Path of the server\'s directory where eDiversa files will be '
        'generated.')


    @classmethod
    def validate(cls, configurations):
        super(Configuration, cls).validate(configurations)
        for configuration in configurations:
            configuration.check_ediversa_path()

    def check_ediversa_path(self):
        if not self.ediversa_path:
            return
        if not os.path.isdir(self.ediversa_path):
            raise UserError(gettext('electrans_account_invoice_ediversa.ediversa_path_not_exists',
                    path=self.ediversa_path))
        if (not os.access(self.ediversa_path, os.R_OK)
                or not os.access(self.ediversa_path, os.W_OK)):
            raise UserError(gettext('electrans_account_invoice_ediversa.ediversa_path_no_permission',
                    path=self.ediversa_path))


class AccountTax(metaclass=PoolMeta):
    __name__ = 'account.tax'
    ediversa_type = fields.Selection([
            ('', ''),
            ('VAT', 'IVA'),
            ('IGI', 'IGIC'),
            ('EXT', 'Exento'),
            ('IMP', 'Importacion'),
            ('RE', 'Recargo de equivalencia'),
            ('withholding', 'Withholding'),
            ], 'eDiversa Type')

    @staticmethod
    def default_ediversa_type():
        return ''


class PaymentType(metaclass=PoolMeta):
    __name__ = 'account.payment.type'
    ediversa_type = fields.Selection([
            ('', ''),
            ('10', 'Cash'),
            ('20', 'Check'),
            ('31', 'Debit Transfer'),
            ('42', 'To Bank Account'),
            ('60', 'I.O.U.'),
            ], 'eDiversa Type',
        help='Used when generate the file to send to eDiversa to generate the '
        'electronic invoice.')
    factoring = fields.Boolean('Factoring',
        help='It is used in eDiversa e-invoice generation.\n'
        'In invoices that use this payment type, the user must to select the '
        'bank\'s account. The bank\'s information is send as endorsee and the '
        'assignment clause is included in invoice.')
    factoring_assignment_clause = fields.Text('Assignment Clause',
        translate=True, size=350, states={
            'invisible': ~Eval('factoring', False),
            'required': Eval('factoring', False),
            }, depends=['factoring'])

    @staticmethod
    def default_ediversa_type():
        return ''


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    recipt_reference = fields.Char(
        "Recipt reference",
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': Eval('type') == 'in'},
        depends=['state', 'type'])

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        cls._buttons.update({
                'generate_ediversa_files': {
                    'invisible': ~Eval('state').in_(['posted', 'paid']),
                    },
                })

    @classmethod
    @ModelView.button
    def generate_ediversa_files(cls, invoices):
        for invoice in invoices:
            invoice.create_ediversa_file()

    def create_ediversa_file(self):
        pool = Pool()
        AccountConfig = pool.get('account.configuration')

        assert self.type in ('out')

        account_config = AccountConfig(1)
        if not account_config.ediversa_path:
            raise UserError(gettext(
                'electrans_account_invoice_ediversa.ediversa_path_not_configured'))

        file_lines = self.get_ediversa_header_lines()

        position = 0
        for line in self.lines:
            if line.type != 'line':
                continue
            position += 1
            file_lines += line.get_ediversa_detail_lines(position)

        file_lines += self.get_ediversa_summary_lines()

        try:
            file_path = os.path.join(account_config.ediversa_path,
                                     'invoice-%s.txt' % self.id)

            with open(file_path, 'w') as output_file:
                for line in file_lines:
                    # if isinstance(line, str):
                    #     line = unicode(line)
                    output_file.write(line + "\r\n")
            logger.info(
                'The eDiversa file "%s" for invoice "%s" is writen '
                'correctly' % (file_path, self.rec_name))
        except Exception as e:
            logger.error(
                    'Error "%s" generating eDiversa file "%s" for invoice '
                    '"%s":\n%s' % (
                        type(e), file_path, self.rec_name,
                        ''.join(traceback.format_exc())
                    ))

    def get_ediversa_header_lines(self):
        lines = [
            'INVOIC_D_1_ED_FACTURAE',
            ]
        # C2 INV
        document_number = self.number
        assert len(document_number) <= 17
        document_type = '380'
        if self.untaxed_amount < 0:
            document_type = '381'
        # TODO: if the invoice is posted for second time (is posted but it
        # already has number), document_type == 384
        function = '9'
        lines.append(
            '|'.join(('INV', document_number, document_type, function)))

        # C3 DTM
        dtm_line_vals = self._get_ediversa_dtm_vals()
        assert dtm_line_vals.get('document')
        assert all(len(v) <= 24 for v in dtm_line_vals.values())
        dtm_line_vals.setdefault('service', '')
        dtm_line_vals.setdefault('period', '')
        dtm_line_vals.setdefault('period1', '')
        dtm_line = ('DTM|%(document)s|%(service)s|||||%(period)s|||%(period1)s'
            % dtm_line_vals)
        lines.append(dtm_line)

        # C4 PAI
        if getattr(self, 'payment_type', False):
            if not self.payment_type.ediversa_type:
                raise UserError(gettext(
                    'electrans_account_invoice_ediversa.missing_ediversa_payment_type',
                        invoice=self.rec_name,
                        payment_type=self.payment_type.rec_name))
            lines.append('PAI|%s' % self.payment_type.ediversa_type)

        # C5 ALI
        # lines.append('ALI|')

        # C50 TXT
        for txt_line_vals in self._get_ediversa_txt_vlist():
            txt_line_vals.setdefault('code', '')
            txt_line = 'TXT|%(text)s|%(type)s|%(code)s' % txt_line_vals
            lines.append(txt_line)

        # C60 RFF
        rff_line_vals = self._get_ediversa_rff_vals()
        if rff_line_vals:
            for line in rff_line_vals:
                assert (line.get('calificator')
                    and line.get('reference'))
                assert len(line['reference']) <= 17
                lines.append('RFF|%(calificator)s|%(reference)s' % line)

        # C70 NADSCO
        nadsco_line_vals = self._get_ediversa_nadsco_vals()
        # TODO: check required files and lenghts
        nadsco_line_vals.setdefault('edi_code', '')
        nadsco_line_vals.setdefault('bank_account', '')
        nadsco_line = ('NADSCO|%(edi_code)s|%(name)s|%(mercantil)s|%(street)s'
            '|%(city)s|%(zip)s|%(tax_identifier)s|%(country_code)s|%(bank_account)s'
            '||%(legal_form)s|%(residense)s') % nadsco_line_vals
        lines.append(nadsco_line)

        # C71 NADBCO
        nadbco_line_vals = self._get_ediversa_nadbco_vals()
        # TODO: check required files and lenghts
        nadbco_line_vals.setdefault('edi_code', '')
        nadbco_line = ('NADBCO|%(edi_code)s|%(name)s|%(street)s|%(city)s'
            '|%(zip)s|%(tax_identifier)s|%(country_code)s|%(legal_form)s'
            '|%(residense)s' % nadbco_line_vals)
        lines.append(nadbco_line)

        if self.party.require_dir3_offices:
            # C75 NADIV
            assert self.party.management_office
            nadiv_line_vals = self._get_ediversa_party_address_vals(
                self.party.management_office.party,
                self.party.management_office, include_dir3_code=True)
            nadiv_line_vals.setdefault('country_code', '')
            nadiv_line_vals.setdefault('dir3_code', '')
            nadiv_line = ('NADIV||%(name)s|%(street)s|%(city)s|%(zip)s'
                '||%(country_code)s|%(dir3_code)s' % nadiv_line_vals)
            lines.append(nadiv_line)

            # C23.10 NADPR
            assert self.party.submitting_office
            nadpr_line_vals = self._get_ediversa_party_address_vals(
                self.party.submitting_office.party,
                self.party.submitting_office, include_dir3_code=True)
            for fname in ('name', 'street', 'city', 'zip', 'country_code',
                        'tax_identifier', 'dir3_code'):
                nadpr_line_vals.setdefault(fname, '')
            nadpr_line = ('NADPR||%(name)s|%(street)s|%(city)s|%(zip)s'
                '|%(country_code)s|%(tax_identifier)s|%(dir3_code)s'
                % nadpr_line_vals)
            lines.append(nadpr_line)

            # C23.31 NADITO
            assert self.party.accounting_office
            nadito_line_vals = self._get_ediversa_party_address_vals(
                self.party.accounting_office.party,
                self.party.accounting_office, include_dir3_code=True)
            for fname in ('edi_code', 'name', 'street', 'city', 'zip',
                        'country_code', 'dir3_code'):
                nadito_line_vals.setdefault(fname, '')
            nadito_line = ('NADITO|%(edi_code)s|%(name)s|%(street)s|%(city)s'
                '|%(zip)s|%(country_code)s|%(dir3_code)s'
                % nadpr_line_vals)
            lines.append(nadito_line)

        # C92 NADDL (required if factoring)
        if (getattr(self, 'payment_type', False)
                and self.payment_type.factoring):
            naddl_line_vals = self._get_ediversa_naddl_vals()
            naddl_line_vals.setdefault('edi_code', '')
            naddl_line = ('NADDL|%(edi_code)s|%(name)s|%(street)s|%(city)s'
                '|%(zip)s|%(country_code)s|%(tax_identifier)s|%(legal_form)s'
                '|%(residense)s|%(bank_account)s'
                % naddl_line_vals)
            lines.append(naddl_line)

        # C100 CTAXXX and COMXXX
        # contact info related to NADXXX, XXX = SCO, BCO, IV, DL or ITO
        if getattr(self, 'contact', False):
            ctabco_line_vals = self._get_ediversa_contact_vals(self.contact)
            ctabco_line = 'CTABCO|IC|%(name)s' % ctabco_line_vals
            lines.append(ctabco_line)
            for com_type, com_value in ctabco_line_vals.items():
                if com_type == 'name':
                    continue
                combco_line = 'COMBCO|%s|%s' % (com_type, com_value)
                lines.append(combco_line)

        # C120 CUX
        cux_line = 'CUX|%s|4' % self.currency.code
        lines.append(cux_line)

        # C130 PAT
        # calificator 1 (retentions) not supported
        pat_lines_vlist = self._get_ediversa_pat_vlist()
        if len(pat_lines_vlist) == 1:
            pat_line = ('PAT|35|%(maturity_date)s|%(amount)s'
                % pat_lines_vlist[0])
            lines.append(pat_line)
        else:
            for pat_line_vals in pat_lines_vlist:
                pat_line = ('PAT|21|%(maturity_date)s|%(amount)s'
                    % pat_line_vals)
                lines.append(pat_line)

        # C160 ALC (global discounts and charges. not used because everything
        # is an invoice line)
        # alc_line = 'ALC|%(calificator)s||%(type)s|%(rate)s|%(amount)s

        return lines

    def _get_ediversa_dtm_vals(self):
        """
        Return a dictionary with next context:
            - document: format %Y%m%d, required
            - service: format %Y%m%d, optional
            - period: format %Y%m%d%Y%m%d, optional
            - period1: format %Y%m%d%Y%m%d, optional
        """
        assert self.invoice_date
        return {
            'document': self.invoice_date.strftime('%Y%m%d'),
            }

    def _get_ediversa_txt_vlist(self):
        vlist = []
        for inv_tax in self.taxes:
            if getattr(inv_tax.tax, 'report_description', False):
                vlist.append({
                        'text': inv_tax.tax.report_description,
                        'type': 'ZZZ',
                        })
        if (getattr(self, 'payment_type', False)
                and self.payment_type.factoring):
            vlist.append({
                    'type': 'AAI',
                    'text': self.payment_type.factoring_assignment_clause,
                    })
        return vlist

    def _get_ediversa_rff_vals(self):
        # Some customers want his order number, understanding there are only one
        # order by invoice (otherwise it had to be used the RFFLIN).
        res = []
        if self.sale_reference:
            res.append({
                'calificator': 'ON',
                'reference': self.sale_reference[:17]})
        if self.recipt_reference:
            res.append({
                'calificator': 'DQ',
                'reference': self.recipt_reference[:17]})
        return res

    def _get_ediversa_nadsco_vals(self):
        inc_bank_account = (getattr(self, 'account_bank', '') == 'company')
        return self._get_ediversa_party_address_vals(self.company.party,
            self.company.party.addresses[0],
            include_bank_account=inc_bank_account)

    def _get_ediversa_nadbco_vals(self):
        return self._get_ediversa_party_address_vals(self.party,
            self.invoice_address)

    def _get_ediversa_naddl_vals(self):
        assert (getattr(self, 'payment_type', False)
            and self.payment_type.factoring)
        assert getattr(self, 'account_bank', '') == 'other'
        bank_party = self.bank_account.bank.party
        return self._get_ediversa_party_address_vals(bank_party,
            bank_party.addresses[0], include_bank_account=True)

    def _get_ediversa_party_address_vals(self, party, address,
            include_bank_account=False, include_dir3_code=False):
        if (not party.tax_identifier or not party.name or not address.street
                or not address.city or not address.postal_code or not address.country):
            raise UserError(gettext(
                'electrans_account_invoice_ediversa.missing_ediversa_party_required_fields',
                    invoice=self.rec_name,
                    party=party.rec_name))
        res = {
            'edi_code': party.tax_identifier.code if party.tax_identifier else '',
            'name': party.name if party.name else '',
            'street': address.street.replace('\n',', ') if address.street else '',
            'city': address.city if address.city else '',
            'zip': address.postal_code if address.postal_code else '',
            'tax_identifier': party.tax_identifier.code if party.tax_identifier else '',
            'country_code': address.country.code.replace('\n',', ') if address.country else '',
            'legal_form': 'J',  # TODO: J(uridica) or F(isica)
            'residense': 'R',  # TODO: R(esidente), E(xtranjero), U(nion Eur)
            'mercantil': self.company.ediversa_mercantil,
            }
        if include_bank_account:
            res['bank_account'] = ''
            if getattr(self, 'bank_account', False):
                for number in self.bank_account.numbers:
                    if number.type == 'iban':
                        res['bank_account'] = number.compact_iban
                        break
        if include_dir3_code:
            res['dir3_code'] = party.dir3_code if party.dir3_code else ''
        return res

    def _get_ediversa_contact_vals(self, party):
        res = {
            'name': party.name,
            }
        if party.phone:
            res['TE'] = party.phone
        elif party.mobile:
            res['TE'] = party.mobile
        if party.fax:
            res['FX'] = party.fax
        if party.email:
            res['EM'] = party.email
        return res

    def _get_ediversa_pat_vlist(self):
        vlist = []
        for move_line in self.move.lines:
            if not move_line.account.type.receivable:
                continue
            vlist.append({
                    'maturity_date': (
                        move_line.maturity_date.strftime('%Y%m%d') if move_line.maturity_date else None),
                    'amount': move_line.debit - move_line.credit,
                    })
        return vlist

    def get_ediversa_summary_lines(self):
        lines = []

        # R20 MOARES
        moares_line_vals = self._get_ediversa_moares_vals()
        moares_line_vals.setdefault('discount_amount', '')
        moares_line_vals.setdefault('charges_amount', '')
        moares_line_vals.setdefault('financial_charges', '')
        moares_line = ('MOARES|%(net_base)s||%(base)s|%(total)s|%(tax_amount)s'
            '|%(discount_amount)s||%(charges_amount)s|%(amount_to_pay)s'
            '||||||||%(withholding_amount)s||||||||||%(financial_charges)s'
            '||%(total_to_pay)s') % moares_line_vals
        lines.append(moares_line)

        # R30 TAXRES
        taxres_line_vlist = self._get_ediversa_taxres_vlist()
        # assert len(taxres_line_vlist) > 0
        for taxres_line_vals in taxres_line_vlist:
            taxres_line = ('TAXRES|%(type)s|%(rate)s|%(amount).2f|%(base).2f'
                % taxres_line_vals)
            lines.append(taxres_line)
        return lines

    def _get_ediversa_moares_vals(self):
        tax_amount = sum(it.amount for it in self.taxes
            if it.tax.ediversa_type != 'withholding')
        if not isinstance(tax_amount, Decimal):
            tax_amount = Decimal('0')
        tax_amount = self.currency.round(tax_amount)

        withholding_amount = sum(it.amount for it in self.taxes
            if it.tax.ediversa_type == 'withholding')
        if not isinstance(withholding_amount, Decimal):
            withholding_amount = Decimal('0')
        withholding_amount = self.currency.round(withholding_amount)

        return {
            'net_base': self.untaxed_amount,
            'base': self.untaxed_amount,
            'total': self.total_amount,
            'tax_amount': tax_amount,
            # 'discount_amount': ,  # invoice (not line) discounts
            # 'charges_amount': ,  # invoice (not line) charges
            'amount_to_pay': self.total_amount,
            'withholding_amount': withholding_amount,
            # 'financial_charges': ,
            'total_to_pay': self.amount_to_pay,
            }

    def _get_ediversa_taxres_vlist(self):
        res = []
        for invoice_tax in self.taxes:
            if not invoice_tax.tax.ediversa_type:
                raise UserError(
                    gettext(
                        'electrans_account_invoice_ediversa.missing_ediversa_tax_type',
                            invoice=self.rec_name,
                            tax=invoice_tax.tax.rec_name))

            if invoice_tax.tax.ediversa_type == 'withholding':
                continue
            res.append({
                    'type': invoice_tax.tax.ediversa_type,
                    'rate': invoice_tax.tax.rate * 100 \
                            if invoice_tax.tax.type == 'percentage' else 0.0,
                    'amount': invoice_tax.amount,
                    'base': invoice_tax.base,
                    })
        return res


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    def get_ediversa_detail_lines(self, position):
        lines = []

        # D10 LIN
        # provably it doesn't work because sequence is not unique
        lin_line = 'LIN|||%s' % position
        lines.append(lin_line)

        # D30 IMDLIN
        description = self.description
        while description:
            if len(description) > 70:
                line_description = description[:70].rsplit(' ', 1)[0]
                description = description[len(line_description):]
            else:
                line_description = description
                description = ''
            imdlin_line = 'IMDLIN|%(description)s||||||%(lang)s' % {
                'description': line_description,
                'lang': (self.invoice.party.lang.code[:2].lower()
                    if self.invoice.party.lang else ''),
                }
            lines.append(imdlin_line)

        # D50 QTYLIN
        qtylin_line = 'QTYLIN|47|%s' % (self.quantity
            if self.quantity else 0.0)
        lines.append(qtylin_line)

        # D70 DTMLIN (service period)
        dtmlin_line_vals = self._get_ediversa_dtmlin_vals()
        if dtmlin_line_vals:
            dtmlin_line = 'DTMLIN||||||%(service_period)s'
            lines.append(dtmlin_line)

        # D90 MOALIN
        moalin_line_vals = self._get_ediversa_moalin_vals()
        moalin_line_vals.setdefault('gross_amount', '')
        moalin_line = ('MOALIN|%(amount)s|||%(gross_amount)s'
            % moalin_line_vals)
        lines.append(moalin_line)

        # D100 PRILIN
        prilin_line_vals = self._get_ediversa_prilin_vals()
        prilin_line = ('PRILIN|%(calificator)s|%(unit_price)s'
            % prilin_line_vals)
        lines.append(prilin_line)

        # D110 RFFLIN
        rfflin_line_vlist = self._get_ediversa_rfflin_vlist()
        for rfflin_line_vals in rfflin_line_vlist:
            rfflin_line = ('RFFLIN|%(calificator)s|%(reference)s'
                % rfflin_line_vals)
            lines.append(rfflin_line)

        # D130
        taxlin_line_vlist = self._get_ediversa_taxlin_vlist()
        # assert len(taxlin_line_vlist) > 0
        for taxlin_line_vals in taxlin_line_vlist:
            taxlin_line = ('TAXLIN|%(type)s|%(rate)s|%(amount).2f|||%(base).2f'
                % taxlin_line_vals)
            lines.append(taxlin_line)

        # D150 ALCLIN
        # Atention: SGAE, pneumatics, batteries and other pseudo-taxes, almost
        # the carrier charges, seems to be specified here.
        # carrier modules compatibility?
        for alclin_line_vals in self._get_ediversa_alclin_vlist():
            alclin_line = ('ALCLIN|%(calificator)s||%(type)s||%(rate)s'
                '|%(amount)s' % alclin_line_vals)
            lines.append(alclin_line)

        return lines

    def _get_ediversa_dtmlin_vals(self):
        # TODO: prepare service period as min(moves.date) and max(moves.date)?
        return {}

    def _get_ediversa_moalin_vals(self):
        res = {
            'amount': self.amount
            }
        if getattr(self, 'discount', False):
            res['gross_amount'] = self.invoice.currency.round(
                Decimal(str(self.quantity)) * self.gross_unit_price)
        return res

    def _get_ediversa_prilin_vals(self):
        res = {
            'subtotal': self.amount
            }
        if getattr(self, 'discount', False):
            return {
                'calificator': 'AAB',
                'unit_price': self.gross_unit_price,
                }
        else:
            return {
                'calificator': 'AAA',
                'unit_price': (self.unit_price
                    if self.unit_price else Decimal('0.0')),
                }
        return res

    def _get_ediversa_rfflin_vlist(self):
        if self.origin and self.origin.__class__.__name__ == 'sale.line':
            return [{
                    'calificator': 'ON',
                    'reference': self.origin.sale.reference,
                    }]
        return []

    def _get_ediversa_taxlin_vlist(self):
        Tax = Pool().get('account.tax')

        res = []
        ediversa_taxes = []
        for tax in self.taxes:
            if not tax.ediversa_type:
                raise UserError(
                    gettext('electrans_account_invoice_ediversa.missing_ediversa_tax_type',
                        invoice=self.invoice.rec_name,
                        tax=tax.rec_name))

            if tax.ediversa_type == 'withholding':
                continue
            ediversa_taxes.append(tax)
        if ediversa_taxes:
            # t = [{amount, base, tax}]
            for t in Tax.compute(ediversa_taxes, self.unit_price,
                    self.quantity):
                tax = t['tax']
                res.append({
                    'type': tax.ediversa_type,
                    'rate': tax.rate * 100 if tax.type == 'percentage' else 0.0,
                    'amount': t['amount'],
                    'base': t['base'],
                    })
        return res

    def _get_ediversa_alclin_vlist(self):
        if getattr(self, 'discount', False):
            discount_amount = self.invoice.currency.round(
                Decimal(str(self.quantity)) * self.gross_unit_price
                * self.discount)
            return [{
                    'calificator': 'D',
                    'type': 'DI',
                    'rate': self.discount * 100,
                    'amount': discount_amount,
                    }]
        return []
