Intración facturas con eDiversa
===============================

Este módulo provee integración con eDiversa: proveedor de factura electrónica.

Genera un fichero plano por cada factura siguiento el formato esperado por los
sistemas de eDiversa.
