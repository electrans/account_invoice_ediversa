Account Invoice eDiversa integration
====================================

Module that provides integration with eDiversa: electronic invoice provider.

It generates a plain text file for each invoice following the format expected
by eDiversa systems.
