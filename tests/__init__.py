# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from .test_electrans_account_invoice_ediversa import suite

__all__ = ['suite']
