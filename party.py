# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.i18n import gettext
from trytond.exceptions import UserError

__all__ = ['Party']


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    public_administration = fields.Boolean('Public Administration')
    require_dir3_offices = fields.Function(
        fields.Boolean('Require DIR3 Offices'),
        'on_change_with_require_dir3_offices')
    management_office = fields.Many2One('party.address', 'Management Office',
        states={
            'invisible': ~Eval('require_dir3_offices', False),
            'required': Eval('require_dir3_offices', False),
            }, depends=['require_dir3_offices'],
        help='Used in electronic invoice.')
    submitting_office = fields.Many2One('party.address', 'Submitting Office',
        states={
            'invisible': ~Eval('require_dir3_offices', False),
            'required': Eval('require_dir3_offices', False),
            }, depends=['require_dir3_offices'],
        help='Used in electronic invoice.')
    accounting_office = fields.Many2One('party.address', 'Accounting Office',
        states={
            'invisible': ~Eval('require_dir3_offices', False),
            'required': Eval('require_dir3_offices', False),
            }, depends=['require_dir3_offices'],
        help='Used in electronic invoice.')
    dir3_code = fields.Char('DIR3 Code', size=35,
        help='Used in electronic invoice.')

    @classmethod
    def validate(cls, parties):
        super(Party, cls).validate(parties)
        for party in parties:
            party.check_offices_dir3_codes()

    def check_offices_dir3_codes(self):
        if (self.management_office
                and not self.management_office.party.dir3_code):
            raise UserError(gettext(
                'electrans_account_invoice_ediversa.missing_dir3_code',
                    party=self.rec_name,
                    office_party=self.management_office.party.rec_name))
        if (self.submitting_office
                and not self.submitting_office.party.dir3_code):
            raise UserError(gettext(
                'electrans_account_invoice_ediversa.missing_dir3_code',
                    party=self.rec_name,
                    office_party=self.submitting_office.party.rec_name))
        if (self.accounting_office
                and not self.accounting_office.party.dir3_code):
            raise UserError(gettext(
                'electrans_account_invoice_ediversa.missing_dir3_code',
                    party=self.rec_name,
                    office_party=self.accounting_office.party.rec_name))

    @fields.depends('public_administration', 'addresses')
    def on_change_with_require_dir3_offices(self, name=None):
        if not self.public_administration:
            return False
        if (not self.addresses or not self.addresses[0].country
                or self.addresses[0].country.code != 'ES'):
            return False
        if not self.addresses[0].subdivision:
            return True
        if self.addresses[0].subdivision.type == 'autonomous community':
            return self.addresses[0].subdivision.code != 'ES-CT'
        elif (self.addresses[0].subdivision.parent
                and self.addresses[0].subdivision.parent.type
                == 'autonomous_community'):
            return self.addresses[0].parent.subdivision.code != 'ES-CT'
        return True
