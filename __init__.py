# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import party
from . import account
from . import company


def register():
    Pool.register(
        party.Party,
        company.Company,
        account.Configuration,
        account.AccountTax,
        account.Invoice,
        account.InvoiceLine,
        module='electrans_account_invoice_ediversa', type_='model')
    Pool.register(
        account.PaymentType,
        module='electrans_account_invoice_ediversa',
        depends=['account_payment_type'], type_='model')
